
import java.util.Scanner;

public class Klawiatura {

    @SuppressWarnings("resource")
	public static String readLine() {
        try {
            return new Scanner(System.in).nextLine().trim();
        } catch (Exception e) {
            return null;
        }
    }


    @SuppressWarnings("resource")
	public static int readInt() {
        try {
            return new Scanner(System.in).nextInt();
        } catch (Exception e) {
            return -1;
        }
    }


    @SuppressWarnings("resource")
	public static double readDouble() {
        try {
            return new Scanner(System.in).nextDouble();
        } catch (Exception e) {
            return -1;
        }
    }

    public static void main(String[] args) {
    	
        System.out.println("Podaj warto��: ");
        int input = Klawiatura.readInt();
        System.out.println("Wczytana wartos�: " + input);
    }
}