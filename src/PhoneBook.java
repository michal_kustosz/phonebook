import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PhoneBook {

// private static List<Person> persons = new ArrayList<>();
	
		private static Person p;
	
//  static {
//        persons.add(new Person("Jan", "Kowalski", LocalDate.of(1990, 12, 30), "586914586"));
//        persons.add(new Person("Anna", "Nowak", LocalDate.of(1989, 6, 3), "123456789"));
//        persons.add(new Person("Monika", "Kwasniewska", LocalDate.of(1980, 5, 25), "658912362"));
//        persons.add(new Person("Slawek", "Grzesiak", LocalDate.of(1980, 6, 25), "658912362"));
//        persons.add(new Person("Marcin", "Dyzma", LocalDate.of(1967, 10, 18), "658912362"));
//        persons.add(new Person("Kasia", "Browiec", LocalDate.of(1981, 9, 25), "658912362"));
//        persons.add(new Person("Tomek", "Nurek", LocalDate.of(1985, 4, 11), "658912362"));
//        persons.add(new Person("Dorota", "Borowik", LocalDate.of(1912, 5, 25), "658912362"));
//        persons.add(new Person("Ania", "Duszek", LocalDate.of(1953, 9, 24), "658912362"));
//    }

    
    public static void main(String[] args) throws IOException {
    	
    	 do{
    		 
    		showMenu();
	    	int wybor = giveMeYourChoise();
	    	
	        switch(wybor){
		        case 1:
		        	addPerson();
		          // break; 
		          
		        case 2:
		        	saveToFile();
		            break;
									     
		        case 3:
		        	loadFromFile();
		            break;
		        
		        default:
		           System.out.println("Nie ma takiego numeru"
		           						+ " wybierz ponownie:)");
		           //return;
	      }   
    }while(true);		//end do{}

 }//end main()


	private static int giveMeYourChoise() {
		
		System.out.println("Enter Your choose:");
		int wybor = Klawiatura.readInt();
		System.out.println("You'r choose= "+ wybor);
		return wybor;
	}

////////////////////////////////////////////////////////////////
	private static void loadFromFile() throws IOException {
		
		FileReader fr = new FileReader("personList.txt");
		BufferedReader bfr = new BufferedReader(fr);
		String linia ;
		int i=0;
		
		while( ( linia=bfr.readLine() ) != null){
			i++;
	        System.out.println(i+ ")  " +linia);
	        //dopuki lini jest rozna od null czyli cos w lini jest
	        //pisz w konsoli zawartosc linii
		}	
			  
	}
/////////////////////////////////////////////////////////////////
//	private static void showListOfPeople() {
//		
//		System.out.println("List of People:");
//		   for(Person p : persons){
//		      System.out.println(p);
//		   }
//	}
//    
//////////////////////////////////////////////////////////////////
private static void saveToFile() throws IOException {
	
	FileReader fr = new FileReader("personList.txt");
	BufferedReader bfr = new BufferedReader(fr);
	
	FileWriter file = new FileWriter("personList.txt",true);
	String linia;
	
	int i=0;
	
	while((linia=bfr.readLine()) != null){
		i++;
        System.out.println(linia);
        //dopuki lini jest rozna od null czyli cos w lini jest
        //pisz w konsoli zawartosc linii
	}
	
	if( ( linia=bfr.readLine() ) == null ){
		
			//System.out.println("  jestem w pustej linii pliku");
			StringBuilder sb = new StringBuilder();//zamien to potem na zwyklego stringa i +
	     	
			sb.append("\n")
			.append(p.getFirstName())
	     	.append(" | ")
	     	.append(p.getLastName())
	     	.append(" | ")
	     	.append(p.getPhoneNumber())
	     	.append(" | ")
	     	.append(p.getDateOfBirth())
	     	.append(" | ");
	     	//zapis.println(sb.toString());
	     	
	     	file.write(sb.toString());
	     		
	     	file.close();	
	     	System.out.println("file saved");
	}
	
 }
//////////////////////////////////////////////////////////////////
    private static void addPerson() {
		
    	//Person p = new Person();
    	System.out.println("Enter Name:");
        String imie = Klawiatura.readLine();
        
        System.out.println("Enter Last Name:");
        String nazwisko = Klawiatura.readLine();
        
        System.out.println("Enter date of birth [yyyy-mm-dd]:");
        String dateOfBirth = Klawiatura.readLine();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDateOfBirth = LocalDate.parse(dateOfBirth, formatter);
        
        System.out.println("Enter telephone number:");
        String telNumber = Klawiatura.readLine();
        
        p = new Person(imie, nazwisko, localDateOfBirth, telNumber);  //dodawanie przez wywolanie konstruktora 
        //tu sie dopiero tworzy obiekt p:Person
    	
	}

////////////////////////////////////////////////////////////////
	private static void showMenu() {
		
		System.out.println("\n*******************");
    	System.out.println("***WELCOME IN MENU***");
    	System.out.println("*********************");
    	System.out.println("****Adresses List:****\n\n");
    	
	    	try {
				loadFromFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	
	    System.out.println();
    	System.out.println("1---> Add Person");
    	System.out.println("2---> Save into File *.txt");
    	//System.out.println("3---> Show list of people");
    	System.out.println("3---> Load from File *.txt");
	}
///////////////////////////////////////////////////////////////////

}