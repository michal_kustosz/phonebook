import java.time.LocalDate;

public class Person implements Comparable<Person>{
	
	private String firstName;
	private String lastName;
	private LocalDate dateOfBirth;
	private String phoneNumber;

	public Person(String firstName, String lastName, LocalDate dateOfBirth, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Person [firstName= " + firstName + ", lastName= " + lastName + ", dateOfBirth= " + dateOfBirth
				+ ", phoneNumber= " + phoneNumber + "]";
	}

	@Override
	public int compareTo(Person o) {
		
		return this.lastName.compareTo(o.lastName);
	}
	
}